## Systemadministraton Vorlesung
#### Softwaredeployment

---
## Webapplikation / Architektur
#### (Statisch)

* HTML
* Javascript
* Bilder

---
## Plattform / Stack

| Webserver | Apache |
| --- | --- |
| Anwendung/Sourcecode | index.html |
| Betriebssystem | Linux |
| Infrastruktur | VM |


---
## Entwicklung
#### Editor (index.html)

```
<!DOCTYPE html>
<html>
<head>
<title>SAM Vorlesung</title>
</head>
<body>

<h1>Hello from SAM </h1>

</body>
</html>
```

---
## Testumgebung
* Browser http://localhost/index.html

---
## Deployment
* rsync
* sftp

---
## Produktivumgebung
#### Plattform / Stack

| Webserver | Apache |
| --- | --- |
| Anwendung/Sourcecode | index.html |
| Betriebssystem | Linux |
| Infrastruktur | VM |

---
## Test / Abnahme
* Browser http://produktiv.sam.at/index.html

---
## Management
#### (Wasserfallmodell)
![Wasserfallmodell](_images/wasserfallmodell.png)

---
## Verbesserung Wartbarkeit / Inhalt
* Static Site Generators

---
## Static Site Generators

Aus statischem Inhalt (meist) Textdateien (Markdown) und einem Template wird ein Statischer Webseitencontent erzeugt , der mittels Webserver ausgeliefert werden kann.

---
## Skizze SSG

![Static Site Generator](_images/ssg-1.png)


---
## JAM Stack
* Javascript
* API
* Markup Language

---
## Eigenschaften
* schnell weil Statischer Content
* Durchaus komplexe Webseiten Möglich (Suche)
* keine echten Dynamsichen Inhalte Möglich (Read Only)
* bekannte Prozesse von der Softwareentwicklung (Build / Versionskontrollsysteme)
* Plattform / Stack bleibt einfach
* Security



---
## SSG Beispiele
* Hugo (https://hugo.io
* Jekyll (https://jekyllrb.com/)
* Pelikan (https://blog.getpelican.com/)

---
## Testumgebung
* Browser http://localhost/index.html

---
## Deployment
* rsync
* sftp

---
## Produktivumgebung
#### Plattform / Stack

| Webserver | Apache |
| --- | --- |
| Anwendung/Sourcecode | index.html,Javascript.js |
| Betriebssystem | Linux |
| Infrastruktur | VM |

---
## Test / Abnahme
* Browser http://produktiv.sam.at/index.html

---
## Management
#### (Wasserfallmodell)
![Wasserfallmodell](_images/wasserfallmodell.png)

---
## Verbesserungen
* (mikro)dynamische Inhalte
* Datenbankanwendungen

---
## Webapplikation (Dynamisch)

#### Wenn eine Webapplikation nicht nur Inhalt darstellen soll , sondern auch mit dem Benutzer interagiert werden muss , wird eine Dynamische Webapplikation benötigt.

---
## Webapplikation Server

#### Als Webapplikationserver wird jener Teil bezeichnet der die Applikation in der jeweiligen Sprache Interpretiert.

---
## Webapplikation Server

* CGI / FCGI
* Webserver Modul
* Webserver Libary

---
## CGI / FCGI
#### (Fast) Comon Gateway Interface
###### Spezifikation einer Schnittstelle zwischen Webserver und Applikationsserver


---
## CGI / FCGI

| Webserver |
| --- |
| CGI / FCGI|
| HTML |
| Applikation |

---
## Eigenschaften

* Standartisiert
* Nicht Performant
* Spezifizierte Schnittstelle
* Probleme bei Skalierung

---
## Webserver Modul
#### mod_php
###### Modul des Webservers um die Applikation direkt Aufrufen zu können.

---
## Webserver Modul

| Webserver |
| --- |
| mod_php |
| HTML |
| Applikation |

---
## Eigenschaften
* besser in der Skalierung
* Performant

---
## Webserver Libary
#### Ruby / Go
###### Einfach zu benutzende Libary in der Applikationssprache um die HTTP Kommunikation direkt zu machen.

---
## Webserver Libary

| Webserverlibary |
| --- |
| HTMLLibary |
| Applikationsroutine |


---
## Eigenschaften
* Performant
* Optimiert
* eventuell komplexe Konfigruation
* Features ?

---
## Plattform / Stack
#### LAMP

| Webserver | Apache |
| --- | --- |
| Anwendung/Sourcecode | ```git clone https:\/\/github.com/kanboard/kanboard``` |
| Applikationsserver | PHP |
| Nutzerdaten | SQLite3 |
| Betriebssystem | Linux |
| Infrastruktur | VM |

---
## Plattform / Stack
#### LEMP

| Webserver | Ngnix |
| --- | --- |
| Anwendung/Sourcecode | ```git clone https:\/\/github.com/kanboard/kanboard``` |
| Applikationsserver | PHP |
| Nutzerdaten | SQLite3 |
| Betriebssystem | Linux |
| Infrastruktur | VM |


---
## Testumgebung
* Lokale Installation
* Browser http://localhost

---
## Deployment Tools
* git
* sftp
* rsync

---
## Deployment Prozess

![Deployment mit GIT](_images/gitops-1.png)

---
## Produktivumgebung
#### Plattform / Stack
###### LAMP

| Webserver | Apache |
| --- | --- |
| Anwendung/Sourcecode | ```git clone https:\/\/github.com/kanboard/kanboard``` |
| Applikationsserver | PHP |
| Nutzerdaten | SQLite3 |
| Betriebssystem | Linux |
| Infrastruktur | VM |

---
## Verbesserung
#### Umgebungen / Testing

Um nicht alle Änderungen der Anwendung direkt beim Kunden testen zu müssen.Werden für verschiedene Aufgaben in der Qualitätssicherung verschiedene Versionen der Anwendung ausgerollt

---
## Eigenschaften

* Unabhängig
* Eigenständig
* so nahe an der Produktivumgebung wie möglich

---
## Beispiele:
* Development
* Testing
* Staging
* Produktion

---
## Deployment Tools
Versionskontrollsysteme

---
## Deployment Prozess

![Deployment GIT Umgebungen](_images/gitops-2.png)

---
## Deployment Prozess


![Deployment GIT Umgebungen](_images/git-deployment-1.png)



---
## Testumgebung
#### Plattform / Stack
###### LAMP

| Webserver | Apache |
| --- | --- |
| Anwendung/Sourcecode | ```git clone https:\/\/github.com/kanboard/kanboard``` |
| Applikationsserver | PHP |
| Nutzerdaten | SQLite3 |
| Betriebssystem | Linux |
| Infrastruktur | VM |

---
## Abnahme / Test
* Browser http://testumgebung.sam.at

---
## Produktivumgebung
#### Plattform / Stack
###### LAMP

| Webserver | Apache |
| --- | --- |
| Anwendung/Sourcecode | ```git clone https:\/\/github.com/kanboard/kanboard``` |
| Applikationsserver | PHP |
| Nutzerdaten | SQLite3 |
| Betriebssystem | Linux |
| Infrastruktur | VM |

---
## Abnahme / Test
* Browser http://produktiv.sam.at


---
## Verbesserungen
* Trennung von Statischen und Dynamischen Inhalten

---
## Vorteile
* Performance
* Skalierung

---
## Nachteile
* Komplexität steigt

---
## Konzept Content Delivery Netzwerk

Um bessere Performance und Latenzen zu erreichen werden Statische und Dynamische Inhalte getrennt und die Statischen Daten über eine getrennte und meist Weltweit verteilte Infrastruktur genannt Content Delivery Netzwerk ausgeliefert.

---
## Eigenschaften Content Delivery Netzwerk
* schnelle Auslieferung von statischem Content
* Weltweit verteilte Infrastruktur

---
## Content
* Bilder
* Statisches Javascript
* Software Updates

---
## Zusätzliche Funktionen
* Caching von Inhalten
* DDOS Protection
* DNS Routing (Global Loadbalancer)

---
## Beispiele CDN
* Varnish (https://varnish-cache.org/)
* Akamai (https://www.akamai.com)
* Cloudflare (https://www.cloudflare.com)

---
## Content Delivery Netzwerk

| Verfügbarkeit | Anycast DNS (BGP) |
| --- | --- |
| Lastverteilung | Loadbalancer L4 - L7 |
| Content | Images |



---
## CDN Übersicht

| Webserver |  | Apache
| --- | --- | --- |
| Content | Statisch | -> CDN |
| Content | Dynamisch | Applikationsserver |
| Nutzerdaten |Datenbank| SQLite3 |
| Betriebssystem | |Linux |
| Infrastruktur | |VM |

---
## CDN 1
![CDN Deployment](_images/deployment-cdn-1.png)

---
## CDN 2
![CDN Deployment](_images/deployment-cdn-2.png)

---
## CDN 3
![CDN Deployment](_images/deployment-cdn-3.png)


---
## Verbesserung
* Trennung des Applikationsstacks

---
## Plattform / Stack

| Webserver |   Apache
| --- | --- |
| Content | PHP |
| Betriebssystem  | Linux |
| Infrastruktur  | VM |

## Netzwerk

| Nutzerdaten |Datenbank (MariaDB) |
|--- | --- |
| Betriebssystem  |Linux |
| Infrastruktur  |VM |

---
## Skalierung mit Funktionstrennung

![Skalierung mit Funktionstrennung](_images/scale-web-o-lb.png)

---
## Vorteile

* Performance
* Skalierung
* höhere Verfügbarkeit
* flexibler bei Updates

---
### Verbesserungen
* Skalierung Vertikal zu Horizontal

---
## Vertikale Skalierung
(Scale UP)

#### Bei der Vertikalen Skalierung werden bei Engpässen in der Performance einfach die Resourcen der Hardware / VM erhöht.

---
## Beispiel:
* 200 User
* 4 GB RAM
* 1 vCPU

---
## Beispiel:
* 400 User
* 8 GB RAM
* 2 vCPU

---
## Eigenschaften
* gut geeignet für Monolithische Applikationen
* Performance steigt leider nicht Linear

---
## Plattform / Stack

1 + N
| Webserver |   Apache |
| --- | --- |
| Content | PHP |
| Betriebssystem  | Linux |
| Infrastruktur  | VM |

## Netzwerk

1 x
| Nutzerdaten |Datenbank (MariaDB) |
|--- | --- |
| Betriebssystem  | Linux |
| Infrastruktur  | VM |

---
## Plattform / Stack

![Skalierung Plattform](_images/skalierung-web-komponenten.png)


---
## Horizontale Skalierung
(Scale OUT)

#### Bei der Vertikalen Skalierung werden nicht die Resourcen für eine Instanz einer Applikation erhöht , sondern die Anzahl der Instanzen.

---
## Skalierung Horizontal

![Skalierung Vertikal](_images/scale-web-vertikal.png)

---
## Konzept Loadbalancer
* Um die Lasten auf die Applikationsserver zu verteilen und dadurch eine bessere Performance zu erreichen
* es werden verschiedene Methoden verwendet um dies zu erreichen
* OSI 7 Layer Modell

---
## Skalierung mit Loadbalancer

![Skalierung mit Loadbalancer](_images/scale-with-lb.png)


---
## Verbesserung
#### Deployment Modelle

---
## Blue/Green Deployment
#### (A/B)

Um die Testumgebung sehr schnell in die Produktivumgebung überführen zu können werden 2  Identische Umgebungen Vorbereitet und über einen Router / Loadbalancer Mechanismus kann die Blue oder Green Version Produktiv geschaltet werden.

---
## Blue/Green Deployment
#### (A/B)
![Skizze Blue/Green Deployment](_images/deployment-blue-green.png)

---
## Eigenschaften
* schnelleres Deployment
* Bei einer Fehlerhaften Version kann wieder auf die "ältere" Version zurückgeschaltet werden.
* Problematisch sind die Datenbanken (Schemaänderung)

---
### Konzept Cannary Deployment
Staging und Produktion Umgebungen wachsen zusammen. Ein kleiner Bereich wird mit der neuen Version des Codes bespielt und getestet. Wird diese für OK befunden werden mehr und mehr User mit dieser Version versorgt.

---
## Voraussetzungen
* Automatisierte Tests
* Automatisiertes Deployment


---
## Skizze Cannary
![Canary Deployment Stage 1](_images/deployment-canary-1.png)

---
## Skizze Cannary 2
![Canary Deployment Stage 2](_images/deployment-canary-2.png)

---
## Skizze Cannary 3
![Canary Deployment Stage 3](_images/deployment-canary-3.png)

---
## Konzept Varianten
* Rolling Release
* Phased rollout
* Incremental rollout.

---
## Umgebungen
* Router (Webapp)
* Loadbalancer
* Aplikation (Feature Toggles)
* Usergruppen

---
## Deployments
#### Summary
* A/B Deployment
* Canary Deployment

---
## Summary Entwicklungsprozess
#### Skizze
* Code
* Build
* Package
* Deploy

---
## Wasserfallmodell

![Wasserfallmodell](_images/wasserfallmodell.png)

---
## Feedback ist Notwendig

---
## Summary Entwicklungsprozess
#### Skizze
* Code
* Build
* Test
* Package
* Deploy

---
## Testing
* Unit Tests
* Integration Tests
* A/B Testing
* End to End Tests
* Smoke Tests

---
## Unit Tests
#### Modultests / Komponententests

Wird verwendet um funktionale Einzelteile einer Software auf die Korrekte Funktion zu testen.

---
## Eigenschaften Unit Tests
* meist erste Teststufe
* begrenzte Komplexität
* wenige Testfälle
* Umfangreiche oder Vollständige Testung möglich
* Kategorie White-box Tests

---
## Integration Tests

Integrationstest Testet das Zusammenspiel der Einzelkomponenten und deren Abhängigkeiten . Die Einzelkomponenten sollten den Modultest schon bestanden haben bevor der Integrationstest gestartet wird.

---
## A/B Testing
Beschreibt eine Feedback Schleife im Deployment und vergleicht die Testergebnisse 2er Versionen um eine Aussage über die Qualität machen zu können.

---
## Skizze A/B Testing
* Kontroll Version (Green) -> Variation (Blue)
* Indikatoren
  * Testabdeckung
  * Testergebnisse

---
## Continious Integration
#### Kontinuierliche Integration

Beschreibt einen Build Prozess der neben dem Erstellen der Applikation auch gleich Unit und Integrationstests durchführt

---
## Continious Intergration
* Code (Local Repository)
* Commit (Zentral Repository)
* Build Server (CI Server)
* Unit Tests
* Integration Tests

---
## Continious Intergration

![Continious Intergration](_images/continious-integration.png)

---
## Continious Deployment

Beschreibt einen Prozess (Pipeline) der nach einem Erfolgreichen Integrationstest Halb oder ganz Automatisch ein Deployment der Applikation durchführt

---
## Continious Deployment
* Code (Local Repository)
* Commit (Zentral Repository)
* Build Server (CI Server)
* Unit Tests
* Integration Tests
* Testumgebung
* End 2 End Tests
* Report
* Manual (Atomatic) Cannary Deployment

---
## Continious Deployment

![](_images/continious-integration-2.png)

---
## Devops
![](_images/devops-symbol.png)


---
## Devsecops
* Automatisierte Security Tests

---
## Security Tests
* statische Analyse
* dynamsiche Analyse
* Vulnerability Scans

---
## Devsecops

![](_images/devsecops-1.png)

---
## Monolithic Application

![Monlitische Aplikation Übersicht](_images/monolithic-app.png)

---
## Konzept Microservices
* Sollte überschaubar sein
* kleine gekapselte Services
* Grundregel in 1 bis 2 Wochen ersetzbar
* Aus der Software Entwicklung
* UNIX Prinzip kleine Unabhänginge Programme / Prozesse / Services die miteinander ein großes ganzes ergeben

---
## Microservices
##### Skizze Architektur
Beispiel:
* 2 x Loadbalancer (Stateless)
* 2 x Webserver (Statecache Redis)
* 1 x Datenbank

---
## Microservices
##### Skizze Funktionen

![Microservices Funktionsweise](_images/microservice-app.png)

---
## Konzept Stateless Application
* Trennt die Daten von der Ausführungsebene / Applikation somit wird ein Austausch der Applikation (Versionsupdate) einfacher
* Einfaches Deployment
* Hoher Automatisierungsgrad
* Hoher Standartisierungsgrad
* Docker / Rocket / LXD
* Docker Konzept Build Ship Run

---
## Webapplikation / Architektur (Summary)
* HTML
* Javascript
* Bilder
* Dynamischer Inhalt (Anwendung)
* Persistente Daten (Datenbank)




---
# Links
Static Site Generators:
* https://hackernoon.com/static-vs-dynamic-website-whats-the-difference-in-2019-327064bfd4a9
Common Gateway Interface
* https://de.wikipedia.org/wiki/Common_Gateway_Interface
* Microservices-Konzeption und Design (Sam Newman)
  ISBN: 978-3-95845-083-7
* Martin Fowler (https://martinfowler.com)
* Blue Green Deployment
https://martinfowler.com/bliki/BlueGreenDeployment.html
